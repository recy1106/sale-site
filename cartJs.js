
var cart = new shopping_cart("jadrn032");
$(document).ready( function() {
	//show cart info
	updateDisplay();
	$("#copyInfockx").checked = false;
//==for initial page====================================
	$.get("/jadrn032/servlet/FetchCategory",function(data){
		var toWrite = "";
		if (data != "invalid") {
		var tmpStr = data.split("||");
		for(i=0; i<tmpStr.length; i++) {
			var tmp = tmpStr[i].split("=");
			toWrite += "<li><a href=\"/jadrn032/servlet/SearchProducts?searchKey=" + tmp[1]+"\" >";
			toWrite += tmp[1]+"</a> </li>";
			}
		}
		$("#categoryInfo").html(toWrite);
	});
	$.get("/jadrn032/servlet/FetchVendor",function(data){
		var toWrite = "";
		if (data != "invalid") {
		var tmpStr = data.split("||");
		for(i=0; i<tmpStr.length; i++) {
			var tmp = tmpStr[i].split("=");
			toWrite += "<li><a href=\"http://jadran.sdsu.edu/jadrn032/servlet/SearchProducts?searchKey=" + tmp[1]+"\" >";
			toWrite += tmp[1]+"</a> </li>";
			}
		}
		$("#vendorInfo").html(toWrite);
	});
	
	$("#vendorInfo").on("click", "a", function(e) {
		e.preventDefault();
		title = $(this).html();
		$.get($(this).attr("href"), function(response) {
			clear_error();
			$(".checkout").hide();
			$("#ptTitle").html(title);
			$("#cartTable").html(response);
			addToCart();
			});
	});
	
	$("#categoryInfo").on("click", "a", function(e) {
		e.preventDefault();
		title = $(this).html();
		$.get($(this).attr("href"), function(response) {
			clear_error();
			$(".checkout").hide();
		 	$("#ptTitle").html(title);
			$("#cartTable").html(response);
			addToCart();
		});
	});
	
	var cartArray = cart.getCartArray();
	var skus = [];
	for(var i = 0; i < cartArray.length; i++){
		skus.push(cartArray[i][0]);
	}
	if(skus.length > 0){
		$("#emptyCart").hide();
		$.get("/jadrn032/servlet/GetCartItems?sku=" + skus.join(","), function(data){
			if(data == "No Items"){
				$("#emptyCart").show();
				$(".checkout").hide();
			} else {
				$("#shoppingCart tbody").html(data);
				for(i = 0; i < cartArray.length; i++){
					$("#qty" + cartArray[i][0]).val(cartArray[i][1]);
				}
				//change values in price area 
				getTotal();
			}
		});
	} else {
		$(".checkout").hide();
	}
	
	// update items quantity
	$("#shoppingCart").on("click", ".update" , function(){
		var qty = parseInt($(this).prev().val());
		var aqty = parseInt($(this).next().attr("value"));
		var sku = $(this).attr("id").substring(6);
		if(!checkQty(sku)){
			write_error("Please input a valid quantity value.");
			return;
		}
		if(qty > aqty){
			$(this).prev().val(aqty.toString());
			qty = aqty;
			write_error("Item ("+sku+") has only "+aqty+" in stock.");
			return;
		}
		cart.setQuantity(sku, qty);
		updateDisplay();
		getTotal();
		write_msg("Item ("+sku+") has been update from your cart.");
	});
	
	// remove items
	$("#shoppingCart").on("click", ".remove" , function(){
		var tmp = this.id.split("||");
		var sku = tmp[1];
		cart.delete(sku);
		$(this).closest("tr").remove();
		updateDisplay();
		//change values in price area 
		getTotal();
		write_error("Item ("+sku+") has been removed from your cart.");
		if(cart.size() == 0){
			$("#emptyCart").show();
			$("#shoppingCart tbody").hide();
			$(".checkout").hide();
		}
	});
	
//================================================
	 //For button "Continue Shoping"
	$("#shoppingBtn").on("click", function(){
        	goHomePage();
	});
	
//================================================
	//For button "Checkout"
	$("#checkoutBtn").on("click", function(){
		clear_error();
		if(!infoCheck(cartArray)){
			write_error("Please input a valid quantity value.");
			return;
		}
        	$("#ui-dialog").dialog("open");
	});
//===============================================
	$(":submit").on("click", function(e) {
		e.preventDefault();
	});
	
//===============================================
	 //For button "Search"
        $("#searchButton").on("click", function() {
        	var inputValue =  $.trim( $('[name="searchKey"]').val() );
		searchPd(inputValue);
	});
	
//===========for open dialog==========
$("#ui-dialog").dialog({
		height: 600,
		width: 700,
		modal: true,
		autoOpen: false,
		buttons: [{text: "Order Comfirm", click: function(){
				//check input info
				if (validateForm()) {
					// for next dialog
					getComfirmInfo();
					$(this).dialog('close');
					$("#checkErr").val("");
					}
				}
			},
			{text: "Cancel", click: function(){
				$("#copyInfockx").checked = false;
				$(".inputInfo").val("");
				$(this).dialog('close');
				$("#checkErr").val("");
				}
			}
		]
	});

	$("#orderConfirm").dialog({
		height: 600,
		width: 700,
		modal: true,
		autoOpen: false,
		buttons: [{text: "Submit", click: function(){
			confirmOrder();
			}//for first text funtion
		},
		{text: "Cancel", click: function(){
			$(this).dialog('close');
			}
		}
		]
	});
	
	$("#copyInfockx").click(function() {
		if($(this).prop('checked')){
			copyShippingToBilling();
		} else {
			clearBillingField();
		}
	});
//==========end dialog==========
	
});  // end document ready
	
// for search button
function searchPd(inputValue){
	$.get("/jadrn032/servlet/SearchProducts?searchKey=" + inputValue, function(data) {
		clear_error();
		$(".checkout").hide();
		$("#ptTitle").html("");
 		$("#cartTable").html(data);
 		write_msg("Search results for \"" + inputValue +"\"");
 	 	addToCart();
		});
	}
	
// for add to cart button
function addToCart(){
	$(".btn-cntl").click(function(){ 
			var tmp = this.id.split("||");
			var sku = tmp[1];
        		cart.add(sku, 1);
			updateDisplay();
			write_msg("Selected Item("+sku+") has been added to your cart.");
		});
	}
	
// input check
function checkQty(sku) {
	//check quantity
	if($("#qty" + sku).val().length==0){
		return false;
		}
	if(!checkQuantity($("#qty" + sku))){
		return false;
		}
	return true;
	}
	
// check quantity number validate
function checkQuantity(element) {
	var value = $.trim($(element).val());
	if(value > 0){
		var pattern = /^[1-9]\d*$/;
		if(value.match(pattern))
			return true;
		return false;
		}
	return false;
	}
	
//error and message write
function write_error(errorMessage) { 
	$("#errorMessage").css("color","red").html(errorMessage);   
	} 

function write_msg(msg){
	$("#errorMessage").css("color","blue").html(msg);
	}
	
// clear message
function clear_error() {
	$("#errorMessage").html("");
	}
	
// display shopping cart value
function updateDisplay() {
	$("#count").html(cart.size());
	} 
	
// get all price
function getTotal(){
	var finalSum = 0;
	var shipping = 0;
	var saleTax = 0;
	var priceTotal = 0;
	var output = "";
	var qty = 0;
	var unitPrice = 0;
	var cartArray = cart.getCartArray();
	for(var i = 0; i < cartArray.length; i++){
		qty = parseFloat(cartArray[i][1]);
		unitPrice = parseFloat($("#price" + cartArray[i][0]).val());
		if(isNaN(unitPrice)){
			unitPrice = 0;
		}
		finalSum = finalSum + qty * unitPrice;
		//each item has $5 shipping fee
		shipping = shipping + qty  * 5;
	}
	
	saleTax = (finalSum + shipping) * 0.08;
	priceTotal = finalSum + shipping + saleTax;
	//display them
	$("#finalSum").html("$" + finalSum.toFixed(2));
	$("#shipping").html("$" + shipping.toFixed(0));
	$("#saleTax").html("$" + saleTax.toFixed(2));
	$("#priceTotal").html("$" + priceTotal.toFixed(2));
}

function goHomePage() {
	window.location.replace("http://jadran.sdsu.edu/jadrn032/proj3.html");
	}
	
// ======check out function==========
function infoCheck(cartArray){
	var flag = "true";
	for(var i = 0; i < cartArray.length; i++){
		flag = checkQty(cartArray[i][0]);
		if (flag == false) {
			return false;
		}
	}
	return true;
}

function copyShippingToBilling(){
	$("#billingInfo [type='text']").each(function(){
		var index=$(this).index("#billingInfo [type='text']");
		var correspondingVal=$("#shippingInfo [type='text']").eq(index).val();
		$(this).val(correspondingVal);
	});
}

function clearBillingField(){
	$("#billingInfo [type='text']").val("");
}

function getComfirmInfo(){
	// for shipping info part
	$("#shippingNm").html($("#shippingName").val());
	$("#shippingAddr").html($("#shippingAddress1").val() + " " + $("#shippingAddress2").val() + " " + $("#shippingCity").val() + " " + $("#shippingState").val() + " " + $("#shippingZip").val());
	$("#shippingTel").html($("#shippingPhone").val());
	$("#billingNm").html($("#billingName").val());
	$("#billingAddr").html($("#billingAddress1").val() + " " + $("#billingAddress2").val() + " " + $("#billingCity").val() + " " + $("#billingState").val() + " " + $("#billingZip").val());
	
	// for product info part
	$("#confirmTable").html($("#shoppingCart").html());
	var cartArray = cart.getCartArray();
	for(i = 0; i < cartArray.length; i++){
		$("#confirmTable #qty"+cartArray[i][0]).val(cartArray[i][1]);
	}
	
	// can't do update or delete
	$("#confirmTable .qtyinput").attr('disabled','disabled');
	$("#confirmTable .update").attr('disabled','disabled');
	$("#confirmTable .remove").attr('disabled','disabled');
	
	
	$("#orderConfirm").dialog('open');
}

// ======confirm function==========
function confirmOrder(){
	var cartArray = cart.getCartArray();
	var skus = [];
	var qtys = [];
	for(var i = 0; i < cartArray.length; i++){
		skus.push(cartArray[i][0]);
		qtys.push(cartArray[i][1]);
	}

	if(skus.length > 0){
	$.get("/jadrn032/servlet/UpdateItemsOnhand?sku=" + skus.join(",") + "&qty=" + qtys.join(","), function(data){
		$("#orderConfirm").dialog('close');
		if(data == "OK"){
			//delete cookies
			for(var i = 0; i < cartArray.length; i++){
    				cart.delete(cartArray[i][0]);
    			}
    			updateDisplay();
    			$("#emptyCart").show();
			$("#shoppingCart tbody").hide();
			$(".checkout").hide();
			write_msg("Congratulations! Your items have been ordered successfully. Thank you for your business.");
		} else {
			write_error("Sorry, There is a problem in our system. Please contact our associate now.");
		}
	});
	} else {
		write_error("Please review your order quantity again.");
	}
}

//check shipping and billing info
function validateForm(){
	if($('#shippingName').val()==''){
		write_error_foc(('#shippingName'), "Please enter a shipping name.");
		return false;
	}
	if($('#shippingAddress1').val()==''){
		write_error_foc($('#shippingAddress1'), "Please enter a shipping address.");
		return false;
	}
	if($('#shippingCity').val()==''){
		write_error_foc($('#shippingCity'), "Please enter a shipping city.");
		return false;
	}
	if($('#shippingState').val()==''){
		write_error_foc($('#shippingState'), "Please enter a shipping state.");
		return false;
	}
	
	if(!checkNums($('#shippingZip').val(), 5)){
		write_error_foc($('#shippingZip'), "Please enter a 5 digit shipping zip code.");
		return false;
	}
	if($('#shippingPhone').val() == ''){
		write_error_foc($('#shippingPhone'), "Please enter a 10 digit shipping phone number.");
		return false;
	}
	
	if($('#billingName').val()==''){
		write_error_foc($('#billingName'), "Please enter a billingName.");
		return false;
	}
	if($('#billingAddress1').val()==''){
		write_error_foc($('#billingAddress1'), "Please enter a billing address.");
		return false;
	}
	if($('#billingCity').val()==''){
		write_error_foc($('#billingCity'), "Please enter a billing city.");
		return false;
	}
	
	if($('#billingState').val()==''){
		write_error_foc($('#billingState'), "Please select a billing state.");
		return false;
	}
	
	if(!checkNums($('#billingZip').val(), 5)){
		write_error_foc($('#billingZip'), "Please enter a 5 digit billing zip code.");
		return false;
	}
	if($('#billingPhone').val() == ''){
		write_error_foc($('#billingPhone'), "Please enter a 10 digit billing phone number.");
		return false;
	}
	
	//validate the credit card info
	if($('#cardType').val()==''){
		write_error_foc($('#cardType'), "Please select a credit card type.");
		return false;
	}

	var securityDigits=3;
	var cardDigits=16;
	if(!checkNums($('#cardNumber').val(), cardDigits)){
		write_error_foc($('#cardNumber'), "Please enter a "+cardDigits+" digit credit card number.");
		return false;
	}
	if(!checkNumRange($('#expirationMonth').val(), 1,12)){
		write_error_foc($('#expirationMonth'), "Please enter a number between 1 and 12 for the expiration month.");
		return false;
	}

	if(!checkNums($('#expirationYear').val(), 4)){
		write_error_foc($('#expirationYear'), "Please enter a 4 digit number for the expiration year.");
		return false;
	}
	if(!checkValidDate($('#expirationMonth').val(), $('#expirationYear').val())){
		write_error_foc($('#expirationMonth'), "That date appears to be invalid.");
		return false;
	}
	if(!checkFutureDate($('#expirationMonth').val(), $('#expirationYear').val())){
		write_error_foc($('#expirationYear'), "Your credit card has expired.");
		return false;
	}
	
	if(!checkNums($('#securityCode').val(), securityDigits)){
		write_error_foc($('#securityCode'), "Please enter the "+securityDigits+" digit security code as seen on your card.");
		return false;
	}
	return true;
}
function checkNums(input, length){
	if(input.length!=length){
		return false;
	}
	for(var i=0;i<input.length;i++){
		if(isNaN(Number(input.charAt(i)))){
			return false;
		}
	}
	return true;
}

function checkNumRange(stringValue, min, max){
	if(!checkNums(stringValue, stringValue.length)){
		return false;
	}
	var num=Number(stringValue);
	if(num < min || num > max){
		return false;
	}
	return true;
}

function checkValidDate(userMonth, userYear){
	var checkDate = new Date(userYear, Number(userMonth)-1, 28);    
	var checkDay = checkDate.getDate();
	var checkMonth = checkDate.getMonth()+1;
	var checkYear = checkDate.getFullYear();
	if(28 == checkDay && userMonth == checkMonth && userYear == checkYear){
		return true;
	}
	return false;
}
// now turn the three values into a Date object and check them
function checkFutureDate(userMonth, userYear){
	if(userMonth.length<2){
		userMonth="0"+(Number(userMonth)-1);
	}
	var checkDate = new Date(userYear, userMonth, 28);    
	var now=new Date();
	if(checkDate>now){
		return true;
	}
	return false;
}

function write_error_foc(element, error) { 
	$("#checkErr").css("color","red").html(error);  
	//$(element).focus();
	} 