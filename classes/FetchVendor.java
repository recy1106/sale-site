
import java.io.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import help.*; 

public class FetchVendor extends HttpServlet {

private static final long serialVersionUID = 1L;
private String nextStep = "";

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		PrintWriter out = response.getWriter();

		String result = "";
		String query = "select vendor_id, description from vendor";
		Vector<String []> answer = DBConnHelper.doQuery(query);
		for(int i=0; i < answer.size(); i++) {
			String [] tmp = answer.elementAt(i);
			for(int j=0; j < tmp.length; j++) {
 				result = result + tmp[j] + "=";
 			}
			result = result + "||";
		}
		
		result = result.substring(0, (result.length() - 2));
		out.print(result);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {
			doGet(request, response);
	}


	private void writeErrorString(PrintWriter out, String info) {
		out.print("Sorry, " + info + " !");
	}
       
}


