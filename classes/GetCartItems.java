
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.*;
import helpers.*;

public class GetCartItems extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		DBHelper db = new DBHelper();
		
		String searchKey = request.getParameter("sku");
		if (searchKey == null || searchKey == "") {
			out.println("No Items");
			return;
		}
		String [] skus = searchKey.split(",");
		Vector<String []> results = new Vector<String []>();
		for(int i = 0; i < skus.length; i++){
			Vector<String []> data = db.doQuery("SELECT p.sku_id, v.description, p.manufacturer_id, p.image_url, p.retail, h.on_hand_quantity" +
				" FROM  (products p INNER JOIN vendor v on p.vender_id = v.vendor_id)" +
			 	" LEFT JOIN on_hand h ON p.sku_id = h.sku_id " +
				"WHERE p.sku_id = '"+skus[i]+"';");
			if(data.size() > 0) {
				results.add(data.elementAt(0));
			}
		}
		
		if (results.size() == 0) {
			out.println("No Items");
			return;
		}
		
		out.println(db.getCartItemTable(results));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
