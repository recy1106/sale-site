
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.*;
import helpers.*;

public class GetFeaturedProducts extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		Vector<String []> answer = DBHelper.doQuery("SELECT p.sku_id, v.description, p.manufacturer_id, p.image_url, p.product_features, p.retail, h.on_hand_quantity " +
				"FROM  (products p inner join vendor v on p.vender_id = v.vendor_id) left join on_hand h on p.sku_id = h.sku_id ORDER BY h.on_hand_quantity DESC LIMIT 8;");
		out.println(DBHelper.getQueryResultTable(answer));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
