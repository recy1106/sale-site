
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import helpers.*;

public class GetProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String sku=request.getParameter("sku");
		DBHelper db = new DBHelper();
		
		Vector<String []> results = db.doQuery("SELECT p.sku_id, v.description, p.manufacturer_id, p.description, p.product_features, p.retail, p.image_url, h.on_hand_quantity, c.description" +
				" FROM  (products p INNER JOIN vendor v on p.vender_id = v.vendor_id INNER JOIN category c ON p.category_id = c.category_id)" +
			 	" LEFT JOIN on_hand h ON p.sku_id = h.sku_id " +
				"WHERE p.sku_id = '"+sku+"'; ");
		if(results.size() == 0) {
			out.print("No item");
			return;
		}
		
		String [] product=results.elementAt(0);
		
		String[] features=product[4].split(";");
		String featureString="";
		for(int i=0;i<features.length;i++){
			featureString=featureString+"<li>"+features[i]+"</li>\n";
		}
		product[4] = featureString;
		
		//check status
		String status = "";
		if (product[7] == null) {
			status = "Comming Soon";
		} else {
			int on_hand = Integer.parseInt(product[7]);
			if(on_hand > 0){
				status = "In Stock";
			}else if(on_hand == 0){
				status = "More On the Way";
			}else{
				status = "Comming Soon";
			}
		}
		product[7] = status;
		out.print(DBHelper.getProduct(product));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
