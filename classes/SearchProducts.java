
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.*;
import helpers.*;

public class SearchProducts extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		DBHelper db = new DBHelper();
		
		String searchKey = request.getParameter("searchKey");
		if (searchKey == null) searchKey = "";
		
		if (searchKey.length() < 2) {
			Vector<String []> answer = db.doQuery("SELECT p.sku_id, v.description, p.manufacturer_id, p.image_url, p.product_features, p.retail, h.on_hand_quantity, c.description" +
					" FROM  (products p INNER JOIN vendor v on p.vender_id = v.vendor_id INNER JOIN category c ON p.category_id = c.category_id)" +
			 		" LEFT JOIN on_hand h on p.sku_id = h.sku_id ORDER BY p.retail DESC;");
			out.println(db.getQueryResultTable(answer));
			return;
		}
		
		Vector<String []> results = db.doQuery("SELECT p.sku_id, v.description, p.manufacturer_id, p.image_url, p.product_features, p.retail, h.on_hand_quantity, c.description" +
				" FROM  (products p INNER JOIN vendor v on p.vender_id = v.vendor_id INNER JOIN category c ON p.category_id = c.category_id)" +
			 	" LEFT JOIN on_hand h ON p.sku_id = h.sku_id " +
				"WHERE p.sku_id = '"+searchKey+"' OR LOWER(c.description) LIKE LOWER('%"+searchKey+"%') " +
				"OR LOWER(v.description) LIKE LOWER('%"+searchKey+"%') OR LOWER(p.manufacturer_id) LIKE LOWER('%"+searchKey+"%') " +
				"ORDER BY p.retail DESC;");
		
		out.println(db.getQueryResultTable(results));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
