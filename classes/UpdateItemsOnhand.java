
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import helpers.*;

public class UpdateItemsOnhand extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		DBHelper db = new DBHelper();
		String result = "";
		int resultCnt = -1;
		String skus = request.getParameter("sku");
		String qtys = request.getParameter("qty");
		Date date = new Date( );
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		String time = ft.format(date);
		if (skus == null || skus == "" || qtys == null || qtys == "") {
			out.println("Error");
			return;
		}
		
		String[] skuArray = skus.split(",");
		String[] qtyArray = qtys.split(",");
		for(int i=0;i<skuArray.length;i++){
			String sku=skuArray[i];
			String orderQtyString=qtyArray[i];
			int orderQty=Integer.parseInt(orderQtyString);

			// do update for each items
			try {
			// check if sku in stock
			Vector<String []> a = db.doQuery("select sku_id from products where sku_id= '" + sku + "';");
			// sku not exist, return with error message
			if(a.size() == 0) {
				writeErrorString(out, "we don't have any product called "+ sku +" in stock");
				return;
			}
			
			// check quantity in hand_on
			int q = 0;
			Vector<String []> answer = db.doQuery("select on_hand_quantity from on_hand where sku_id= '" + sku + "';");
			if(answer.size() == 0)  {
				// sku dose not exist, do insert
				writeErrorString(out, "we don't have any product called "+ sku +" in stock");
				return;
			} else {
				// sku dose exist, do update
				q = getFixQuantity(answer, orderQtyString);
				if (q < 0) {
					q = Integer.valueOf(orderQtyString)+q;
					writeErrorString(out, "we only have "+ q +" products in stock");
					return;
				}
			}
			
			// insert data in merchandise_out
			resultCnt = db.doUpdate("insert into merchandise_out values('" + sku + "', "+ time +", '" + q +"');");
			if (resultCnt == 1) {
				//insert or update data in on_hand
				resultCnt = db.doUpdate("update on_hand set last_date = NOW(), on_hand_quantity =" + q + " where sku_id= '" + sku + "';");
				if (resultCnt == 1) {
					result = "OK";
				} else {
					result = "errorOnhand";
				}
			} else {
				result = "error";
			}
			
			} catch (Exception e) {
				writeErrorString(out, "Error happened when doing update " + e);
				return;
			}
		}//for loop
		out.print(result);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	private void writeErrorString(PrintWriter out, String info) {
		out.print("Sorry, " + info + " !");
	}
       
	private int getFixQuantity(Vector<String []> answer, String inputQuan) {
		int quantity = -1;
		for(int i=0; i < answer.size(); i++) {
			String [] tmp = answer.elementAt(i);
			for(int j=0; j < tmp.length; j++) {
				quantity = Integer.valueOf(tmp[0]) - Integer.valueOf(inputQuan);
 			}
		}
		return quantity;
	}
}
