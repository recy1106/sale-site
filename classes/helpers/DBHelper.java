package helpers;

/*
 *     DB connect help method changed from sample 
*/ 

import java.util.*;
import java.sql.*;

public class DBHelper implements java.io.Serializable {
    
// This method is for queries that return a result set.  The returned
// vector holds the results.    
	public static Vector doQuery(String s) {
		String user = "jadrn032";
		String password = "pipe";
		String database = "jadrn032";
		String connectionURL = "jdbc:mysql://opatija:3306/" + database +
      	      "?user=" + user + "&password=" + password;		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		Vector<String []> v = new Vector<String []>();        

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(connectionURL);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(s);
			ResultSetMetaData md = resultSet.getMetaData();
			int numCols = md.getColumnCount();
		
			while(resultSet.next()) {
				String [] tmp = new String[numCols];
				for(int i=0; i < numCols; i++)
					tmp[i] = resultSet.getString(i+1);  // resultSet getString is 1 based
				v.add(tmp);                
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
        	// IMPORTANT, you must make sure that the resultSet, statement and connection
		// are closed, or a memory leak occurs in Tomcat. 
			try {
				resultSet.close();
				statement.close();                
				connection.close();
			} catch(SQLException e) {}  // don't do anything if the connection is not open.
		}
		return v;
	}
    
// This method is appropriate for DB operations that do not return a result 
// set, but rather the number of affected rows.  This includes INSERT and UPDATE    
	public static int doUpdate(String s) {
		String user = "jadrn032";
		String password = "pipe";
		String database = "jadrn032";
		String connectionURL = "jdbc:mysql://opatija:3306/" + database +
      	      "?user=" + user + "&password=" + password;		
		Connection connection = null;
		Statement statement = null;
		int result = -1;   

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(connectionURL);
			statement = connection.createStatement();  
			result = statement.executeUpdate(s);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
		// IMPORTANT, you must make sure that the statement and connection
		// are closed, or a memory leak occurs in Tomcat.
			try {
				statement.close();                
				connection.close();
			} catch(SQLException e) {}  // don't do anything if the connection is not open.
		}
		return result;
	}              

// This method is to build products table for the home page
	public static String getQueryResultTable(Vector<String []> v) {
		StringBuffer toReturn = new StringBuffer();
		toReturn.append("<table id='featuredTb'>");
		for(int i=0; i < v.size(); i++) {
			String [] tmp = v.elementAt(i);
			toReturn.append("<tr>");
			// for each record: sku, vendor, mid, picUrl, features, price, onHandQuantity
			// column left
			toReturn.append("<td>");
			// add sku tmp[0]
			toReturn.append("<a href='/jadrn032/product.html?sku=" + tmp[0]+"'>");
			//add image tmp[3]
			toReturn.append("<img width='125px' height='125px' src='/~jadrn032/ajax_upload/_uploadPicDIR_/" + tmp[3] + "' />");
			toReturn.append("</a>");
			toReturn.append("</td>");
			
			// column center
			toReturn.append("<td>");
			// add sku tmp[0]
			toReturn.append("<a href='/jadrn032/product.html?sku=" + tmp[0]+"'>");
			// add vendor tmp[1] mid tmp[2]
			toReturn.append("<h4>" + tmp[1] + "-" + tmp[2] + "</h4>");
			toReturn.append("</a>");
			toReturn.append("<h5>SKU: " + tmp[0] +"</h5>");
			//check on_hand_quantity tmp[6]
			String status = "";
			if (tmp[6] == null) {
				status = "Comming Soon";
			} else {
				int on_hand = Integer.parseInt(tmp[6]);
				if(on_hand > 0){
					status = "In Stock";
				}else if(on_hand == 0){
					status = "More On the Way";
				}else{
					status = "Comming Soon";
				}
			}
			
			toReturn.append("<h5>Status: " + status +"</h5>");
			toReturn.append("</td>");
			// column right
			//add retail price tmp[5]
			toReturn.append("<td><h4>Price: $" + tmp[5] + "</h4>");
			//add sku to butotn id
			if (status == "In Stock") {
				toReturn.append("<input type='button' class='btn-cntl' value='Add to Cart'  id='addBtn||"+tmp[0]+"'/>");
			}else{
				toReturn.append("<input type='button' class='btn-cntl' disabled='disabled' value='Add to Cart'  id='addBtn||"+tmp[0]+"'/>");
			}
			toReturn.append("</td>");
			toReturn.append("</tr>");
		}
		toReturn.append("</table>"); 
		return toReturn.toString();
	} 
    
// This method is to get one product
	public static String getProduct(String [] tmp){

		StringBuffer toReturn = new StringBuffer();
		//p.sku_id 0, v.description 1, p.manufacturer_id 2, p.description 3, p.product_features 4, 
		//p.retail 5, p.image_url 6, h.on_hand_quantity 7, c.description 8
		toReturn.append("<table>");
		toReturn.append("<tr>");
		toReturn.append("<td>");
		toReturn.append("<div id='productImgContainer'>");
		toReturn.append("	<img id='selectedProductImg' width='300px' height='300px' src='/~jadrn032/ajax_upload/_uploadPicDIR_/" + tmp[6]+"' />");
		toReturn.append("</div>");
		toReturn.append("</td>");
		toReturn.append("<td>");
		toReturn.append("<div id='selectedProductInfo'>");
		toReturn.append("<h2  id='vendorMid'>"+tmp[1] + " " + tmp[2]+"</h2>");
		toReturn.append("<p>");
		toReturn.append("	<h4 id='status'>"+tmp[7]+"</h4>");
		toReturn.append("	<div id='price'>Price: $"+tmp[5]+"</div>");
		toReturn.append("<div>");
		toReturn.append("	<input type='text' size='2' value='1' id='qty' name='qty'/>");
		if (tmp[7] == "In Stock") {
			toReturn.append("	<input type='button' value='Add to Cart' id='addButton' />");
		}else{
			toReturn.append("	<input type='button' disabled='disabled' value='Add to Cart' id='addButton' />");
		}
		
		toReturn.append("	<input type='hidden' id='sku' name='sku' value='"+tmp[0]+"'/>");
		toReturn.append("</div>");
		toReturn.append("<p>");
		toReturn.append("	<h4>Description:</h4>");
		toReturn.append("	<div>"+tmp[3]+"</div>");
		toReturn.append("</p>");
		toReturn.append("</div>");
		toReturn.append("</td>");
		toReturn.append("</tr>");
		toReturn.append("<tr>");
		toReturn.append("<td colspan='2'>");
		toReturn.append("<div id='selectedProductInfo'>");
		toReturn.append("	<h4 id='featuresHeader'>Features:</h4>");
		toReturn.append("	<ul id='features'>"+tmp[4]+"</ul>");
		toReturn.append("</div>");
		toReturn.append("</td>");
		toReturn.append("</tr>");
		toReturn.append("</table>");
		return toReturn.toString();
	}

// This method is to get one product
	public static String getCartItemTable(Vector<String []> v){

		StringBuffer toReturn = new StringBuffer();
		for(int i=0; i < v.size(); i++) {
			String [] tmp = v.elementAt(i);
			//p.sku_id 0, v.description 1, p.manufacturer_id 2, p.image_url 3, p.retail 4, h.on_hand_quantity 5
			toReturn.append("<tr class='productItm'>");
            	toReturn.append("	<td class='picSC'><img class='thumb' src='/~jadrn032/ajax_upload/_uploadPicDIR_/" + tmp[3]+"' ></td>");
            	toReturn.append("	<td class='itemSC'><a href='/jadrn032/product.html?sku=" + tmp[0]+"'>" + tmp[1] + "-" + tmp[2] +"</a></td>");
            	toReturn.append("	<td class='priceSC'>"+tmp[4]+"</td>");
            	toReturn.append("	<td class='qtySC'><input type='text' value='1' min='0' max='3' class='qtyinput' id='qty"+tmp[0]+"' />");
            	toReturn.append("	<input class='update' type='button' value='Update' id='update"+ tmp[0] + "' />");
            	toReturn.append("	<input type='hidden' id='onhand" +tmp[0] + "' name='onhand" +tmp[0] + "' value='"+tmp[5]+"' />");
            	toReturn.append("	<input type='hidden' id='price" +tmp[0] + "' name='price" +tmp[0] + "' value='"+tmp[4]+"' />");
            	toReturn.append("	</td>");
            	toReturn.append("	<td class='delSC'><input class='remove' type='button' value='Del' id='removeBtn||" + tmp[0] + "' /></td>");
         		toReturn.append("</tr>");
		}
		// calculate price info for init
		toReturn.append("	<tr class='priceInfo'>");
		toReturn.append("		<td colspan='2'>&nbsp;</td>");
		toReturn.append("		<td>Product Total<br/>Shipping<br/>Estimated Sales Tax</td>");
		toReturn.append("		<td colspan='2'><h3><div id='finalSum'></div><div id='shipping'></div><div id='saleTax'></div></h3></td>");
		toReturn.append("	</tr>");
		toReturn.append("	<tr class='total'>");
		toReturn.append("		<td colspan='2'>&nbsp;</td>");
		toReturn.append("		<td>Estimated Total</td>");
		toReturn.append("		<td colspan='2'><h3><div id='priceTotal'></div></h3></td>");
		toReturn.append("	</tr>");
		return toReturn.toString();
	}


} 
