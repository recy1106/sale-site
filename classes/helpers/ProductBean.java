package helpers;

 /*
 *    ProductBean.java
 */    

public class ProductBean implements java.io.Serializable {
	
	private String sku;
	private String category;
	private String vendor;
	private String manufacturerId;
	private String description;
	private String features;
	private String price;
	private String imgPath;
	private String status;
	
	public ProductBean(){
	}
	public String getSku(){
		return sku;
	}
	public String getCategory(){
		return category;
	}
	public String getVendor(){
		return vendor;
	}
	public String getManufacturerId(){
		return manufacturerId;
	}
	public String getDescription(){
		return description;
	}
	public String getFeatures(){
		return features;
	}
	public String getPrice(){
		return price;
	}
	public String getImgPath(){
		return imgPath;
	}
	public String getStatus(){
		return status;
	}
	
	public void setSku(String s){
		sku=s;
	}
	public void setCategory(String s){
		category=s;
	}
	public void setVendor(String s){
		vendor=s;
	}
	public void setManufacturerId(String s){
		manufacturerId=s;
	}
	public void setDescription(String s){
		description=s;
	}
	public void setFeatures(String s){
		features=s;
	}
	public void setPrice(String s){
		price=s;
	}
	public void setImgPath(String s){
		imgPath=s;
	}
	public void setStatus(String s){
		status=s;
	}
}