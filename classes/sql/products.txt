 # contents of file products_table_data.txt
use jadrn032;
DROP TABLE IF EXISTS products;
CREATE TABLE products(
	sku_id CHAR(7) NOT NULL PRIMARY KEY,
	category_id int NOT NULL,
	vender_id int NOT NULL,
	manufacturer_id VARCHAR(20) NOT NULL,
	description VARCHAR(300),
	product_features VARCHAR(600),
	cost DECIMAL(6,2) NOT NULL,
	retail DECIMAL(6,2) NOT NULL,
	image_url VARCHAR(200)
	);
