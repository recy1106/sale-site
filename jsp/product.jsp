<!doctype html>

<html lang="en">
<head>
	<%@ page import="java.util.*, helpers.*" %>
	<%@ page errorPage="/proj3.html" %>
	<title>Dream Health</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="/jadrn032/mainCss.css" />
	<link rel="stylesheet" type="text/css" href="/jadrn032/ui/css/redmond/ui.css" />
	
	<script type = "text/javascript" src = "http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type = "text/javascript" src = "http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<!--<script type = "text/javascript" src="/jadrn032/ui/js/jquery-1.10.2.js"></script>
	<script type = "text/javascript" src="/jadrn032/ui/js/jquery_ui_custom.js"></script>-->
	<script type="text/javascript" src="/jadrn032/shopping_cart.js"></script>
	<script type="text/javascript" src="/jadrn032/mainJs.js"></script>
	<script type="text/javascript" src="/jadrn032/productJs.js"></script>
</head>

<body onunload="">
<div id="header">
	<h1><i>Dream's Health Website</i></h1>
</div>

<div id="wrapper">   
	<div id="content">
	<div id="search">
		<div>
		<input class="input-cntl" type="text" id="searchKey" name="searchKey" maxlength="90" placeholder="Search by Keyword, SKU # or Item #" />
		<input type="button" value="Search" id="searchButton" />
		</div>
		<div id="cart"><span id="count">0</span></div>
	</div>
	
	<ul id="menuSelector">
		<li><a href="http://jadran.sdsu.edu/jadrn032/proj3.html"><span>HOME</span></a></li>
		<li><a href="http://jadran.sdsu.edu/jadrn032/cart.html"><span>MY CART</span></a></li>
	</ul>
	<div id="errorMessage"></div>
	<div id="categoryDisplay">
	<p>
		<h3>Shop Brand</h3>
		<ul>
			<div id="categoryInfo"></div>
		</ul>
	</p>
	<p>
		<h3>Shop Vendor</h3>
		<ul>
			<div id="vendorInfo"></div>
		</ul>
	</p>
	
	</div>
	<div id="mainDisplay">
	<div id="productDisplay">
		<jsp:useBean id="productBean" type="helpers.ProductBean" scope="request" />
		<table>
		<tr>
			<td>
			<div id='productImgContainer'>
				<img id='selectedProductImg' width='300px' height='300px' src='/~jadrn032/ajax_upload/_uploadPicDIR_/<jsp:getProperty name="productBean" property="imgPath" />' />
			</div>
			</td>
			<td>
			<div id='selectedProductInfo'>
			<h2><jsp:getProperty name="productBean" property="vendor" /> <jsp:getProperty name="productBean" property="manufacturerId" /></h2>
			<p>
				<h4 id="status"><jsp:getProperty name="productBean" property="status" /></h4>
				Price: $<jsp:getProperty name="productBean" property="price" /></p>
			<div>
				<input type='text' size='2' value='1' id='qty' name='qty'/>
				<input type='button' value='Add to Cart' id='addButton' />
				<input type='hidden' id='sku' name='sku' value='<jsp:getProperty name="productBean" property="sku" />' />
			</div>
			<p>
				<h4>Description:</h4>
				<jsp:getProperty name="productBean" property="description" />
			</p>
			</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<div id='selectedProductInfo'>
				<h4 id='featuresHeader'>Features:</h4>
				<ul>
				<jsp:getProperty name="productBean" property="features" />
				</ul>
			</div>
			</td>
		</tr>
		</table>

	</div><!--for productDisplay-->
	
	<!--for tab My Cart-->
	<div id="tab2">
	<div id="cartInfo"></div>
	<input type="button" class="btn-cntl" id="open" name="open_dialog" value="Open Dialog" />
	<div id="ui-dialog" title="Modal Dialog Box Example">
	<div class="ui-dialog-content">
	This is a modal dialog box.  You can put whatever you want in it.
	</div>
	</div>
	
	</div>
	</div><!--for mainDisplay-->
    
    </div>
</div>


<div id="footer">
    <p>Online prices and selection generally match our retail stores, but may vary. Prices and offers are subject to change. </p><p>© Copyright Dream's website, 2014. All rights reserved. </p>
</div>  
</body>
</html>