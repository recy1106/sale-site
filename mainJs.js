
var cart = new shopping_cart("jadrn032");
$(document).ready( function() {
	//show cart info
	updateDisplay();
	
//==for initial page====================================
	$.get("/jadrn032/servlet/FetchCategory",function(data){
		var toWrite = "";
		if (data != "invalid") {
		var tmpStr = data.split("||");
		for(i=0; i<tmpStr.length; i++) {
			var tmp = tmpStr[i].split("=");
			toWrite += "<li><a href='/jadrn032/servlet/SearchProducts?searchKey=" + tmp[1]+"' >";
			toWrite += tmp[1]+"</a> </li>";
			}
		}
		$("#categoryInfo").html(toWrite);
	});
	$.get("/jadrn032/servlet/FetchVendor",function(data){
		var toWrite = "";
		if (data != "invalid") {
		var tmpStr = data.split("||");
		for(i=0; i<tmpStr.length; i++) {
			var tmp = tmpStr[i].split("=");
			toWrite += "<li><a href='/jadrn032/servlet/SearchProducts?searchKey=" + tmp[1]+"' >";
			toWrite += tmp[1]+"</a> </li>";
			}
		}
		$("#vendorInfo").html(toWrite);
	});
	
	$("#vendorInfo").on("click", "a", function(e) {
		e.preventDefault();
		title = $(this).html();
		$.get($(this).attr("href"), function(response) {
			clear_error();
			$("#ptTitle").html(title);
			$("#featuredPt").html(response);
			addToCart();
			});
	});
	
	$("#categoryInfo").on("click", "a", function(e) {
		e.preventDefault();
		title = $(this).html();
		$.get($(this).attr("href"), function(response) {
			clear_error();
		 	$("#ptTitle").html(title);
			$("#featuredPt").html(response);
			addToCart();
		});
	});
	
	$.get("/jadrn032/servlet/GetFeaturedProducts",function(data){
		$("#ptTitle").html("Featured Computer");
		$("#featuredPt").html(data);
		addToCart();
	});
	
//===============================================
	$(":submit").on("click", function(e) {
		e.preventDefault();
	});
	
//===============================================
	 //For button "Search"
        $("#searchButton").on("click", function() {
        	var inputValue =  $.trim( $('[name="searchKey"]').val() );
		searchPd(inputValue);
	});
	
});  // end document ready
	
// for search button
function searchPd(inputValue){
	$.get("/jadrn032/servlet/SearchProducts?searchKey=" + inputValue, function(data) {
		clear_error();
		$("#ptTitle").html("");
 		$("#featuredPt").html(data);
 	 	write_msg("Search results for \"" + inputValue +"\"");
 	 	addToCart();
		});
	}
	
// for add to cart button
function addToCart(){
	$(".btn-cntl").click(function(){ 
			var tmp = this.id.split("||");
			var sku = tmp[1];
        		cart.add(sku, 1);
			updateDisplay();
			write_msg("Selected Item("+sku+") has been added to your cart.");
		});
	}
//error and message write
function write_error(errorMessage) { 
	$("#errorMessage").css("color","red").html(errorMessage);   
	} 

function write_msg(msg){
	$("#errorMessage").css("color","blue").html(msg);
	}
	
// clear message
function clear_error() {
	$("#errorMessage").html("");
	}
	
// display shopping cart value
function updateDisplay() {
	var cartArray = cart.getCartArray();
	$("#count").html(cart.size());
	} 
//
function searchProduct(key){
	var url = window.location.search.replace("?", "");
	var params = url.split("&");
	for (var i = 0; i < params.length; i++){
		var param = params[i].split("=");
		if(param[0] == key){
			return param[1];
		}
	}
	return "";
}